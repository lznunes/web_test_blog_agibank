from pages.base_page import BasePage
from selenium.webdriver.common.by import By

class ResultElements(object):
    SEACH_RESULT = (By.CLASS_NAME, 'page-title')
    SEACH_NOT_FOUND_TEXT = (By.CLASS_NAME, 'no-results')


class ResultPage(BasePage):
        
    def search_result_title(self):
        return self.driver.find_element(*ResultElements.SEACH_RESULT).text
    
        
    def search_label_not_fount(self):
        label_not_found = self.driver.find_element(*ResultElements.SEACH_NOT_FOUND_TEXT).text
        return label_not_found    
    