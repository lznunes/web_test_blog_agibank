from pages.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class SearchElements(object):
    SEARCH_BUTTON = (By.CLASS_NAME, 'ast-search-icon')
    SEARCH_INPUT = (By.ID, 'search-field')
    SEACH_TERMO =  (By.XPATH, '//article[1]/div/div[2]/article[1]/h3/a')

class SearchPage(BasePage):

    def search_button_click(self):
        self.driver.find_element(*SearchElements.SEARCH_BUTTON).click()
    
    def search_input_sentence(self, sentence):
        self.driver.find_element(*SearchElements.SEARCH_INPUT).send_keys(sentence)
    
    def search_input_sentence_enter(self):
        self.driver.find_element(*SearchElements.SEARCH_INPUT).send_keys(Keys.ENTER)

    def search_titulo(self):
        titulo_primeiro_link = self.driver.find_element(*SearchElements.SEACH_TERMO).text
        return titulo_primeiro_link    

    