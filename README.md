# WEB TESTES AGIBANK

## Webe tests AGIBANK usando BDD testes com Python, Pytest e Selenium frameworks

## Cenários

* Busca por termo no blog

## Ferrmaentas e Linguagem

* Python 3.8
* VSCode

## Passos para executar testes localmente

### Clonar o repositório do Git

```bash
#!/bin/bash
git clone https://gitlab.com/lznunes/web_test_blog_agibank.git
```

### Abrir VSCode no diretório do projeto

### Abrir o terminal prompt

### Configurar ambiente virtual com comandos abaixo

```bash
#!/bin/bash
  python3 -m venv .venv
```

```bash
#!/bin/bash
  source .venv/bin/activate
```

### Instalar dependêncies com comando abaixo

```bash
#!/bin/bash
 pip install -r requirements.txt
```

### Rodar testes com comando abaixo

```bash
#!/bin/bash
python -m pytest -k "todos"
```

## Referências

Pytest Documentaion [ https://docs.pytest.org ]